package com.example.ar

import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.net.toFile
import com.google.ar.core.Anchor
import com.google.ar.sceneform.AnchorNode
//import com.google.ar.sceneform.assets.RenderableSource
import com.google.ar.sceneform.rendering.ModelRenderable
import com.google.ar.sceneform.rendering.RenderableInstance
import com.google.ar.sceneform.ux.ArFragment
import com.google.ar.sceneform.ux.TransformableNode


class MainActivity : AppCompatActivity() {

    private var arFragment: ArFragment? = null
    private var modelRenderable: ModelRenderable? = null

    //3d model credit : google.poly.com
    private val Model_URL = "https://storage.googleapis.com/ar-answers-in-search-models/static/Tiger/model.glb"
    private val Model_LOCAL_URL = "astronaut.glb"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        arFragment = supportFragmentManager.findFragmentById(R.id.arFragment) as ArFragment
        arFragment!!.setOnTapPlaneGlbModel(Model_URL, object: ArFragment.OnTapModelListener{
            override fun onModelAdded(renderableInstance: RenderableInstance?) {
                Toast.makeText(this@MainActivity, "Model added", Toast.LENGTH_SHORT).show()
            }

            override fun onModelError(exception: Throwable?) {
                Toast.makeText(this@MainActivity, "Model can't be Loaded", Toast.LENGTH_SHORT).show()
            }
        })
//        setUpModel()
//        setUpPlane()
    }

    private fun setUpModel() {
        ModelRenderable.builder()
                .setSource(this, Uri.parse(Model_LOCAL_URL))
                .setRegistryId(BuildConfig.APPLICATION_ID)
                .build()
                .thenAccept{
                    modelRenderable = it
                }
                .exceptionally {
                    Log.i("Model", "cant load");
                    Toast.makeText(this, "Model can't be Loaded", Toast.LENGTH_SHORT).show()
                    return@exceptionally null
                }
        }

    private fun setUpPlane(){
        arFragment!!.setOnTapArPlaneListener { hitResult, plane, motionEvent ->
            val anchor = hitResult.createAnchor();
            val anchorNode = AnchorNode(anchor);
            anchorNode.setParent(arFragment!!.arSceneView.scene);
            createModel(anchorNode);
        }
    }

    private fun createModel(anchorNode: AnchorNode){
        val node = TransformableNode(arFragment!!.transformationSystem);
        node.setParent(anchorNode)
        node.renderable = modelRenderable
        node.select()
    }
}